#!bin2/bash
#Este programa hace una monitorización sencilla de la máquina dando por
#pantalla la Ip, la memoria Ram y el espacio del disco duro.
#Además, metemos el programa en una variable que lo saque por un servidor.
informe=/var/www/html/informe.html
echo "                                                           " >>$informe
echo "===========================================================" >>$informe
echo "                      INFORME GENERAL                      " >>$informe
echo "===========================================================" >>$informe
IP=$(sudo ifconfig eth0 | grep "inet addr" |cut -d: -f2 |cut -d" " -f1) >>$informe
echo La ip de esta máquina es: ${IP} >>$informe
echo "-----------------------------------------------------------" >>$informe
RAM=($(free -h |grep Mem)) >>$informe
echo "La memoria ram total que tiene es:" ${RAM[1]} >>$informe
echo "siendo" ${RAM[2]} " la memoria utilizada" >>$informe
echo " y" ${RAM[3]} " la memoria disponible actualmente." >>$informe
echo "----------------------------------------------------------" >>$informe
DISCOS=($(df -h | grep aufs)) >>$informe
echo "Tiene un disco duro de" ${DISCOS[1]} " de almacenamiento" >>$informe
echo "estando en uso" ${DISCOS[2]} >>$informe
echo "y teniendo todavía libre" ${DISCOS[3]} "." >>$informe
echo "-----------------------------------------------------------" >>$informe
#FIN DEL PROGRAMA.
#Nota: He concedido permisos totales al archivo /var/www/html/informe.html para que no de errores.
#Nota: Como he programado en un ordenador por cable, la ipconfig llama a eth0 y no wlan0.
