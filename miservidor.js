//19 de Octubre de 2018.
//Final de curso.
//Este es el servidor EXPRESS que presento como examen.
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

//Bloque 1.
//Comprobamos el estado de la conexión.
function connError(res){
    console.log ('Ha habido un fallo de conexión a esta url: ', url);
    res.send( {ok:false, mensaje: 'Express no puede conectarse.'} );
}

//Bloque 2.
//Definimos la clase que incluye los métodos para el CRUD.
class App{

    static creaDocumento(){                    //CREATE
        if (!mongoCli){ 
            connError(res) 
        }
        else {    
            mongoCli.db(req.params.db).collection(req.params.collection)
              .insertOne(
                { _id: req.params.id }, 
                (err, results) => { 
                  res.send(results ? results : err);
                });  
        }
    };

    static leeDocumento(){                   //READ
        if (!mongoCli) { connError(res) }
        else { mongoCli.db(req.params.db).collection(req.params.collection)
                .findOne(
                { _id: req.params.id }, 
                    (err, results) => { 
                    res.send(results ? results : err);
                });  
        }

    };

    static actualizaDocumento(){            //UPDATE
        if ( !mongoCli ){ connError(res) } 
        else { 
                mongoCli.db(req.params.db).collection(req.params.collection).replaceOne(
                    { _id: id}, req.body, {upsert: true},
                        (err, data) => { res.send(data ? data : err) 
                    });
        }
    };

    static borraDocumento(){                //DELETE
        if ( !mongoCli ){ 
            connError(res) } 
        else { 
                mongoCli.db(req.params.db).collection(req.params.collection)
                .deleteOne(
                    { _id: new mongo.ObjectID(req.params.id) }, 
                    (err, data) => { res.send( data ? data : err) 
                });
        }
    };

    static muestraColeccion(req, res){        
        if ( !mongoCli ){ connError(res) 
        }
        else {
            mongoCli.db(req.params.db).collection(req.params.collection).find().sort()
            .toArray( (err, results) => { 
              res.send(results ? results : err)
            });
        }  
    }
}

//Hasta aquí, estas funciones permiten realizar el CRUD gravias a que los datos
//llegan en REQ.PARAMS.
//MongoCli viene definido abajo. Es el segundo parámetro de la callback
//del método .connect de MONGO.MONGOCLIENT
//----------------------------------------------------------------------------

//Bloque 3.
//Cargamos las las librerías EXPRESS, MONGODB, BODY-PARSER
//y la función EXPRESS() para poder trabajar con sus diferentes métodos.
const express = require('express');
const mongo = require('mongodb');
const bodyParser = require('body-parser');
const app = express();

//El método .connect() de MongoClient nos permite conectarnos a la url que le pasemos.
//Si no se conecta nos muestra un error.
//Pero si logra conectarse, mete el cliente en MONGOCLI para usarlo en el CRUD.
mongo.MongoClient.connect(url, (err, client) => {
    if ( client ){ mongoCli = client}
    else { 
      console.log('hay un error en MongoClient.connect()');
      console.log(err);
    }
});
//-----------------------------------------------------------------------------

//Bloque 4.
//Definimos los métodos por donde se reciben los datos.
app.get('/:db/:collection/:id', App.muestraDocumento);
app.get('/:db/:collection', App.muestraColeccion);
app.post('/:db/:collection', App.creaDocumento);
app.post('/:db/:collection', App.actualizaDocumento);
app.post('/:db/:collection/:id', App.borraDocumento);


//Bloque 5.
//Habilitamos el puerto por el que va a escuchar.
app.listen(port, function() {
    console.log(`Se ha iniciado este servidor en el puerto ${port}.`);
})