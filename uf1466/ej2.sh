#!bin/bash
#Este programa hace copias de seguridad y las saca a las 23.00 gracias a crontab.
rsync -av /home/user/ /backups #Hace la copia remota de los documentos personales de los usuarios.
rsync -av /var/www/html/ /backups #Hace la copia remota de lo que haya publicado en el servidor.
mysqldump -uroot -psecret wordpress >  /backups/wordpress.sql #Hace la copia remota de Wordpress. 
mysqldump -uroot -psecret clientes > /backups/clientes.sql #Hace la copia remota de la base de datos de mysql.
#He programado CRONTAB en el ordenador: * 23 * * * /bin/bash /home/user/ej2.sh
#FIN DEL PROGRAMA.
