<!DOCTYPE html>
<!-- Página 2 del proyecto.
Aquí se pintan las mismas características que en la página 1 y dibuja un formulario
para que un usuario nuevo se registre.-->
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" 
        rel="stylesheet" 
        integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" 
        crossorigin="anonymous">
        <script src="main.js"></script>
    <title>Crea una cuenta</title>
    <style>
        body {background-color: rgb(101, 178, 250);}    
        h3{color: rgb(3, 113, 146);}         
    </style>
</head>
<body>
    <h1 class="container text-center text-primary">Books Lovers</h1>
        <div class="container text-center">
            <img src="https://images.pexels.com/photos/159866/books-book-pages-read-literature-159866.jpeg?auto=compress&cs=tinysrgb&h=350">
        </div>        
        <br><br>
    <h3 class="container text-center text-primary">Crea tu cuenta</h3>
    <?php
            include 'crudusuario.php'; //Te deriva al programa donde se hace el CRUD para que
                                       // un usuario nuevo se registre.

            if(!empty ($_GET['titulo']) && !empty ($_GET['autor']) && !empty ($_GET['titulo']) ){
                $entradaNueva = new Lecturas($_GET['titulo'] && $_GET['autor']);
                $entradaNueva->nuevoRegistro();
            }else {
    ?>
    <form>
            <input type="text" name="<?= $_GET['nombre']?>" placeholder="Nombre" />
            <input type="text" name="<?= $_get['apellidos']?>" placeholder="Apellidos" />
            <input type="email" name="<?= $_GET['email']?>" placeholder="Email" />
            <input type="password" name="<?= $_GET['contraseña'] ?>" placeholder="Contraseña" />
            <input type="sumbit" name="guardar" value="Guardar" />
    </form> 
    <?php    
       }
    ?>        
</body>
</html>