<!--Página 5.
    Este programa hace el CRUD de la pestaña INICIO de mi proyecto. 
    La Base de Datos se llama BOOKLOVERS 
    y la tabla con la que trabaja es LECTURAS.-->

<?php
    //Bloque 1. Nos conectamos a la Base de Datos BOOKLOVERS.
    //Si por alguna razón, la conexión fallase, se nos avisa.
    //Además, especificamos los caracteres castellanos con set_charset.
    $conexion = new mysqli('localhost', 'root', 'a', 'booklovers');
    if ($conexion->connect_error)
        die ('Fallo de conexión con la base de datos');
    $conexion->set_charset('utf8');

    //Bloque 2.
    //Creamos la clase Lecturas que va a contar con 5 métodos, siendo estos
    //el CONSTRUCTOR, donde definiremos las variables con las que vamos a trabajar,
    //y los cuatro siguientes los métodos que componen el CRUD.
    class Lecturas{
        function __construct($titulo, $autor, $editorial){
            $this->id = 0;
            $this->titulo = $titulo;
            $this->autor = $autor;
            $this->editorial = $editorial;
            $this->resumen = $resumen;
        }
        
        function nuevoRegistro(){
            global $conexion;
            $sql = $conexion->prepare('insert into lecturas values (0,?,?,?,?);');
            $sql->bind_param('ssss', $this->titulo,$this->autor,$this->editorial,$this->resumen);
            $sql->execute();
        }

        static public function leerRegistro($id){
            $objeto = new Lecturas ('', '', '', '');
            global $conexion;
            $sql = $conexion->prepare('select * from lecturas where id = ?;');
            $sql->bind_param('i', $id);
            $sql->execute();
            $sql->bind_result($objeto->id, $objeto->titulo, $objeto->titulo,
                              $objeto->editorial, $objeto->resumen);
            $sql->fetch();
            return $objeto;
        }
        
        function actualizarRegistro(){
            global $conexion;
            $sql = $conexion->prepare('update lecturas set titulo = ?, autor = ?,
                                        editorial = ?, reumen = ? where id = ?;');
            $sql->bind_param('ssssi', $this->titulo,$this->autor,
                                      $this->editorial,$this->resumen,$this->id );
            $sql->execute();
        }
        
        function eliminarRegistro(){
            global $conexion;
            $sql = $conexion->prepare('delete from lecturas where id = ?;');
            $sql->bind_param('i', $this->id);
            $sql->execute();
        }
    }
?>
