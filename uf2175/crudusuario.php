<!--Página 3.
    Este programa hace el CRUD para que se registren nuevos usuarios o modifiquen sus datos.
-->

<?php
    //Bloque 1. Nos conectamos a la Base de Datos USUARIOS.
    //Si por alguna razón, la conexión fallase, se nos avisa.
    //Además, especificamos los caracteres castellanos con set_charset.
    $conexion = new mysqli('localhost', 'root', 'a', 'booklovers');
    if ($conexion->connect_error)
        die ('Fallo de conexión con la base de datos');
    $conexion->set_charset('utf8');

    //Bloque 2.
    //Creamos la clase Usuarios que va a contar con 5 métodos, siendo estos
    //el CONSTRUCTOR, donde definiremos las variables con las que vamos a trabajar,
    //y los cuatro siguientes los métodos que componen el CRUD.
    class Usuarios{
        function __construct($nombre, $apellidos, $email, $contraseña){
            $this->id = 0;
            $this->titulo = $nombre;
            $this->autor = $apellidos;
            $this->editorial = $email;
            $this->resumen = $contraseña;
        }
        
        function nuevoRegistro(){
            global $conexion;
            $sql = $conexion->prepare('insert into  values (0,?,?,?,?);');
            $sql->bind_param('ssss', $this->nombre,$this->apellidos,$this->email,$this->contraseña);
            $sql->execute();
        }

        static public function leerRegistro($id){
            $objeto = new Lecturas ('', '', '', '');
            global $conexion;
            $sql = $conexion->prepare('select * from lecturas where id = ?;');
            $sql->bind_param('i', $id);
            $sql->execute();
            $sql->bind_result($objeto->id, $objeto->nombre, $objeto->apellidos,
                              $objeto->email, $objeto->contraseña);
            $sql->fetch();
            return $objeto;
        }
        
        function actualizarRegistro(){
            global $conexion;
            $sql = $conexion->prepare('update usuarios set nombre = ?, apellidos = ?,
                                        email = ?, contraseña = ? where id = ?;');
            $sql->bind_param('ssssi', $this->nombre,$this->apellidos,
                                      $this->email,$this->contraseña,$this->id );
            $sql->execute();
        }
        
        function eliminarRegistro(){
            global $conexion;
            $sql = $conexion->prepare('delete from usuarios where id = ?;');
            $sql->bind_param('i', $this->id);
            $sql->execute();
        }
    }
?>
