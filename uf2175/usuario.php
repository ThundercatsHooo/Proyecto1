<!DOCTYPE html>
<!-- 
    Si el usuario ya está registrado puede acceder a sus entradas o escribir una nueva.
    Aquí te pinta el formulario para escribir una nueva entrada y hace la llamada al
    programa 'crud.php' que permite hacer las funciones propias de CRUD.
-->
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" 
        rel="stylesheet" 
        integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" 
        crossorigin="anonymous">
    <script src="main.js">
    </script>
    <title>Crea una cuenta</title>
    <style>
            body {background-color: rgb(101, 178, 250);}    
            h1{color: rgb(3, 113, 146);}       
          </style>
</head>
<body>
        <h1 class="container text-center text-primary">Books Lovers</h1>
        <div class="container text-center">
            <img src="https://images.pexels.com/photos/159866/books-book-pages-read-literature-159866.jpeg?auto=compress&cs=tinysrgb&h=350">
        </div>        
        <br><br>
        <?php
            include 'crud.php';

            if(!empty ($_GET['titulo']) && !empty ($_GET['autor']) && !empty ($_GET['titulo']) ){
                $entradaNueva = new Lecturas($_GET['titulo'] && $_GET['autor']);
                $entradaNueva->nuevoRegistro();
            }else {
        ?>
        <h3> ¿Quieres escribir una entrada nueva? </h3>
    <form>
        <input type="text" name=" <?=$_GET['titulo']?>" value="" placeholder="Título del libro" />
        <input type="text" name=" <?=$_get['autor']?>" value="" placeholder="Autor del libro" />
        <input type="text" name=" <?=$_GET['editorial']?>" value="" placeholder="Editorial del libro" />
        <input type="text" name=" <?=$_GET['resumen']?>" value="" placeholder="Reseña del libro" />
        <input type="sumbit" name="guardar" value="Guardar" />
    </form>
        <?php    
            }
        ?>
    
</body>
</html>